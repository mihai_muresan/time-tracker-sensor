var ApiService = (function () {
    var config = require('./../config.service.js');
    var httpService = require('./../core/http.service.js');

    const API_URL = config.api.url,
        CLOCK_ENTRY_URL = API_URL + '/clock-entry',
        USER_URL = API_URL + '/user',
        CONNECT_URL = API_URL + '/poa/connect',
        DISCONNECT_URL = API_URL + '/poa/disconnect',
        FINISH_ENROLL_URL = API_URL + '/user/finish-enroll',
        UPLOAD_URL = API_URL + "/poa/upload",
        TEMPLATE_URL = API_URL + "/poa/backup";

    constructor();

    return {
        createClockEntry: createClockEntry,
        createUser: createUser,
        connect: connect,
        disconnect: disconnect,
        finishEnroll: finishEnroll,
        upload: upload,
        getBackup: getBackup
    };

    function constructor() {

    }

    function createUser(sensorId) {
        var data = {
            sensorId: sensorId
        };

        return httpService.createRequest(USER_URL, 'POST', data);
    }

    function createClockEntry(sensorId, type) {
        var data = {
            sensorId: sensorId
        };

        return httpService.createRequest(CLOCK_ENTRY_URL, 'POST', data);
    }

    function connect() {
        return httpService.createRequest(CONNECT_URL, 'POST', {});
    }

    function disconnect(ip) {
        return httpService.createRequest(DISCONNECT_URL, 'POST', {ip: ip});
    }

    function finishEnroll(userId, sensorId) {
        var data = {
            sensorId: sensorId,
            userId: userId
        };

        return httpService.createRequest(FINISH_ENROLL_URL, 'POST', data);
    }

    function upload(sensorId, template) {
        var data = {
            sensorId: sensorId,
            template: template
        };

        return httpService.createRequest(UPLOAD_URL, 'POST', data);
    }

    function getBackup() {
        return httpService.createRequest(TEMPLATE_URL, 'GET', {});
    }
})();

module.exports = ApiService;