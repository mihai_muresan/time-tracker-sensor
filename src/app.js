'use strict';

class App {
    constructor() {
        this._expressServer = require('./express.server');
        this._poaManager = require('./poa/poa.manager');
        this._menuManager = require('./menu/menu.manager');
    }

    /**
     * Starts the application
     */
    start() {
        this._expressServer.start();
        this._poaManager.start();
    }
}

(function main() {
    let app = new App();

    app.start();
})();