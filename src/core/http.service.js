var HttpService = function () {
    this.request = require('request');
};

/**
 * Create a HTTP request
 *
 * @param url       Url where to make the request
 * @param method    The method of the request
 * @param data      Data to send
 * @returns {Promise}
 */
HttpService.prototype.createRequest = function (url, method, data) {
    var self = this;

    return new Promise((resolve, reject)=> {
        self.request(url, {
            method: method,
            json: data
        }, function (err, res, body) {
            if (err || res.statusCode !== 200) {
                reject(body);
                return;
            }

            resolve(body);
        });
    });
};

HttpService.prototype.extractIp = function (req) {
    var ipv6 = req.connection.remoteAddress;
    var ipv4Index = ipv6.lastIndexOf(':') + 1;

    return ipv6.substring(ipv4Index);
};

module.exports = new HttpService();
