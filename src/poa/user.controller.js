'use strict';

class UserController {
    constructor() {
        var self = this;

        var express = require('express');
        var expressServer = require('../express.server');
        var router = express.Router();

        this._httpService = require('../core/http.service');
        this._poaManager = require('./poa.manager');

        router.post('/sync', (req, res) => {
            self._syncUser(req, res);
        });

        router.post('/un-sync', (req, res) => {
            self._unSyncUser(req, res);
        });

        expressServer.addRoute('/user', router);
    }

    _syncUser(req, res) {
        var neighborIp = this._httpService.extractIp(req);
        var encodedTemplate = req.body.template;

        console.log('Got new user from ' + neighborIp);

        this._poaManager.syncUser(neighborIp, req.body.sensorId, encodedTemplate)
            .then(()=> {
                res.json({success: true});
            }, () => {
                res.json({success: false});
            });
    }

    _unSyncUser(req, res) {
        var neighborIp = this._httpService.extractIp(req);

        console.log('Removing user from ' + neighborIp);

        this._poaManager.unSyncUser(neighborIp, req.body.sensorId)
            .then(()=> {
                res.json({success: true});
            }, () => {
                res.json({success: false});
            });
    }
}

module.exports = new UserController();