'use strict';

var sleep = require('sleep');
var VERIFY_NEIGHBORS_INTERVAL = 5000;

class PoaManager {
    constructor() {
        this._poaService = require('./poa.service');
        this._commands = [];

        this.neighbors = {
            leftNeighbor: '',
            rightNeighbor: ''
        };
    }

    start() {
        var self = this;

        console.log('Connecting to API...');

        this._init()
            .then(function () {
                console.log('Sensor initialized');
                console.log('Waiting finger...');

                return self._poaService.backup();
            })
            .then(function () {
                console.log('Backup complete');

                self._run();
                self._startNotifyNeighbors();
            }, function (error) {
                console.error(error);
            });
    }

    pushCommand(name, data) {
        this._commands.push({
            name: name,
            data: data
        });
    }

    updateNeighbor(neighborIp, neighborType) {
        switch (neighborType) {
            case 'LEFT':
                this.neighbors.leftNeighbor = neighborIp;
                break;
            case 'RIGHT':
                this.neighbors.rightNeighbor = neighborIp;
                break;
        }
    }

    syncUser(neighborIp, sensorId, encodedTemplate) {
        var self = this;
        var template = new Buffer(encodedTemplate, 'base64');

        return new Promise((resolve, reject) => {
            self._commands.push({
                name: 'new_fingerprint',
                data: {
                    sensorId: sensorId,
                    template: template
                }
            });

            console.log(encodedTemplate);

            if (neighborIp === self.neighbors.leftNeighbor) {
                self._poaService.syncNeighbors(sensorId, template, [self.neighbors.rightNeighbor]);
            }

            if (neighborIp === self.neighbors.rightNeighbor) {
                self._poaService.syncNeighbors(sensorId, template, [self.neighbors.leftNeighbor]);
            }

            resolve();
        });
    }

    unSyncUser(neighborIp, sensorId) {
        var self = this;

        return new Promise((resolve, reject) => {
            self._commands.push({
                name: 'remove_fingerprint',
                data: {
                    sensorId: sensorId
                }
            });

            if (neighborIp === self.neighbors.leftNeighbor) {
                //self._poaService.syncNeighbors(sensorId, template, [self.neighbors.rightNeighbor]);
                self._poaService.unSyncUser(sensorId, [self.neighbors.rightNeighbor]);
            }

            if (neighborIp === self.neighbors.rightNeighbor) {
                //self._poaService.syncNeighbors(sensorId, template, [self.neighbors.leftNeighbor]);
                self._poaService.unSyncUser(sensorId, [self.neighbors.leftNeighbor]);
            }

            resolve();
        });
    }

    _init() {
        return this._poaService.connectApi()
            .then((response) => {
                this.neighbors.leftNeighbor = response.leftNeighbor;
                this.neighbors.rightNeighbor = response.rightNeighbor;

                console.log('Connected to API');
                console.log(`Neighbors: Left - ${response.leftNeighbor}`);
                console.log(`Neighbors: Left - ${response.rightNeighbor}`);

                console.log('Initializing sensor...');
                return this._poaService.initSensor();
            }, (error) => {
                console.error('Could not connect to API');
                console.error(error);
                process.exit(1);
            })
    }

    _run() {
        var self = this;

        //sleep.sleep(1);

        var cycle = function () {
            console.log('Cycle');

            sleep.sleep(1);

            self._poaService.identify()
                .then(function (sensorId) {
                    console.log('User identified as: ' + sensorId);
                    sleep.sleep(1);
                    cycle();
                }, function (error) {
                    if (error === 'timeout!') {
                        console.log('Sensor not responding.');

                        sleep.sleep(1);

                        self._poaService.stop()
                            .then(function () {
                                console.log('Reinitializing sensor');
                                return self._poaService.initSensor();
                            })
                            .then(function () {
                                cycle();
                            })
                            .catch(function (error) {
                                self.start();
                            });
                        return;
                    }

                    if (!self._commands.length) {
                        cycle();
                        return;
                    }

                    self._executeCommands()
                        .then(function () {
                            cycle();
                        });
                });
        };

        cycle();
    };

    _startNotifyNeighbors() {
        var self = this;

        setInterval(function () {
            self._poaService.notifyNeighbor('RIGHT', self.neighbors.leftNeighbor)
                .then(() => {
                }, (error) => {
                    self.neighbors.leftNeighbor = '';
                });
            self._poaService.notifyNeighbor('LEFT', self.neighbors.rightNeighbor)
                .then(() => {
                }, (error) => {
                    self.neighbors.rightNeighbor = '';
                });
        }, VERIFY_NEIGHBORS_INTERVAL);
    }

    _enrollNewUser(userId) {
        var self = this;

        return new Promise((resolve, reject) => {
            self._poaService.enroll(userId)
                .then((sensorId) => {
                    console.log(`Enrolled at id: ${sensorId}`);
                    
                    self._poaService.syncUser(sensorId, [self.neighbors.leftNeighbor, self.neighbors.rightNeighbor])
                        .then(() => {
                            return self._poaService.blink(2);
                        })
                        .then(() => {
                            resolve();
                        });
                }, (error) => {
                    resolve();
                });
        });
    }

    _executeCommands() {
        var self = this;

        return new Promise((resolve, reject) => {
            var executeCommand = function () {
                if (self._commands.length) {
                    var command = self._commands.pop();
                    var promise;
                    var data = command.data;

                    sleep.sleep(1);
                    console.log('Executing command: ' + command.name);

                    switch (command.name) {
                        case 'enroll':
                            promise = self._enrollNewUser(data.userId);
                            break;
                        case 'un_enroll':
                            promise = self._unEnrollUser(data.sensorId);
                            break;
                        case 'flush':
                            promise = self._poaService.flush();
                            break;
                        case 'new_fingerprint':
                            promise = self._poaService.enrollWithTemplate(data.sensorId, data.template);
                            break;
                        case 'remove_fingerprint':
                            promise = self._poaService.unEnroll(data.sensorId);
                            break;
                    }

                    promise.then(function () {
                        sleep.sleep(2);
                        executeCommand();
                    }, function (error) {
                        sleep.sleep(2);
                        console.log('Could not execute command, error: ' + error);
                        self._commands.push(command);
                        executeCommand();
                    });
                } else {
                    console.log('No commands to execute');
                    resolve();
                }
            };

            executeCommand();
        });
    }

    _unEnrollUser(sensorId) {
        var self = this;

        return new Promise((resolve, reject) => {
            self._poaService.unEnroll(sensorId)
                .then(() => {
                    return self._poaService.unSyncUser(sensorId, [self.neighbors.leftNeighbor, self.neighbors.rightNeighbor]);
                })
                .then(() => {
                    resolve();
                })
                .catch((error) => {
                    resolve();
                })
        });
    }
}

module.exports = new PoaManager();