'use strict';

var express = require('express');
var expressServer = require('../express.server');

class CommandController {
    constructor() {
        var self = this;

        this._poaManager = require('./poa.manager');
        this._httpService = require('../core/http.service');

        var router = express.Router();

        router.post('/execute', (req, res) => {
            self._command(req, res);
        });

        router.post('/notify', (req, res) => {
            self._notify(req, res);
        });

        expressServer.addRoute('/command', router);
    }

    _command(req, res) {
        var command = req.body.command;
        var data = req.body.data;

        this._poaManager.pushCommand(command, data);

        res.json(req.body);
    }

    _notify(req, res) {
        var neighborIp = this._httpService.extractIp(req);
        var neighborType = req.body.type;

        this._poaManager.updateNeighbor(neighborIp, neighborType);

        res.json({message: 'pong'});
    }
}

module.exports = new CommandController();