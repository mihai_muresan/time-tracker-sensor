'use strict';

var sleep = require('sleep');

var POA_PORT = 8080;

class PoaService {
    constructor() {
        this.log = require('winston');
        this.sensorService = require('./../sensor/sensor.service.js');
        this.apiService = require('./../api/api.service.js');
        this.httpService = require('./../core/http.service.js');
    }

    notifyNeighbor(type, ip) {
        var self = this;

        return new Promise((resolve, reject) => {
            if (!ip) {
                resolve();
                return;
            }

            var url = `http://${ip}:${POA_PORT}/command/notify`;

            this.httpService.createRequest(url, 'POST', {type: type})
                .then(() => {
                    resolve();
                }, (error) => {
                    console.log('Could not reach ip: ' + ip);

                    self._disconnectNeighbor(ip);
                    reject(error);
                });

        });
    }

    /**
     * Send user information to neighboring POAs
     *
     * @param sensorId  Sensor id of the user to sync
     * @param neighbors
     */
    syncUser(sensorId, neighbors) {
        console.log(`Syncing user: ${sensorId}`);

        return this.sensorService.getFingerPrint(sensorId)
            .then((template) => {
                var encodedTemplate = template.toString('base64');

                // Upload new employee template to API
                this.apiService.upload(sensorId, encodedTemplate);

                for (var i = 0; i < neighbors.length; i++) {
                    if (neighbors[i]) {
                        this._syncPoa(neighbors[i], sensorId, encodedTemplate);
                    }
                }
            }, (error) => {
                console.log('Could not retrieve template, error: ' + error);
            });
    }

    unSyncUser(sensorId, neighbors) {
        console.log(`Un-Syncing user: ${sensorId}`);

        var self = this;

        return new Promise((resolve, reject) => {
            for (var i = 0; i < neighbors.length; i++) {
                if (neighbors[i]) {
                    self._unSyncPoa(neighbors[i], sensorId);
                }
            }

            resolve();
        });
    }

    syncNeighbors(sensorId, template, neighbors) {
        var encodedTemplate = template.toString('base64');

        for (var i = 0; i < neighbors.length; i++) {
            if (neighbors[i]) {
                this._syncPoa(neighbors[i], sensorId, encodedTemplate);
            }
        }
    }

    connectApi() {
        return this.apiService.connect()
            .then((response) => {
                return response;
            });
    }

    initSensor() {
        var self = this;

        process.on('SIGINT', function () {
            console.log('exit');

            self.sensorService.stop()
                .then(function () {
                    console.log('Success');
                    process.exit(0);
                }, function (error) {
                    console.log('Error');
                    process.exit(0);
                });
        });

        return this.sensorService.start();
    }

    stop() {
        return this.sensorService.stop();
    }

    identify() {
        var self = this;

        return (new Promise(function (resolve, reject) {
            self.sensorService.identify()
                .then(function (sensorId) {
                    self.apiService.createClockEntry(sensorId);

                    resolve(sensorId);
                }, function (error) {
                    reject(error);
                });
        }));
    }

    enroll(userId) {
        var self = this;

        return (new Promise(function (resolve, reject) {
            var tryEnroll = function () {
                self.sensorService.enroll()
                    .then((sensorId)=> {
                        self.apiService.finishEnroll(userId, sensorId);

                        resolve(sensorId);
                    }, () => {
                        tryEnroll();
                    });
            };

            tryEnroll();
        }));
    }

    unEnroll(sensorId) {
        return this.sensorService.removeFingerPrint(sensorId);
    }

    blink(nrOfTimes) {
        return this.sensorService.blink(nrOfTimes);
    }

    flush() {
        console.log('Deleting database...');
        return this.sensorService.removeAll()
            .then(function () {
                console.log('Database cleared');
            });
    }

    enrollWithTemplate(sensorId, template) {
        console.log('Enrolling new user, id: ' + sensorId, template);

        return this.sensorService.setFingerPrint(sensorId, template);
    }

    backup() {
        var self = this;

        return new Promise(function (resolve, reject) {
            console.log('Retrieving backup');

            self.flush()
                .then(function () {
                    return self.apiService.getBackup();
                })
                .then(function (backup) {
                    console.log('Applying backup');

                    if (!backup || backup.length === 0) {
                        console.log('No templates to save');
                        resolve();
                        return;
                    }

                    var saveItem = function (index) {
                        if (index > backup.length - 1) {
                            resolve();
                            return;
                        }

                        var item = backup[index];

                        var sensorId = item.user.sensorId;
                        var template = new Buffer(item.template, 'base64');

                        self.enrollWithTemplate(sensorId, template)
                            .then(function () {
                                index++;
                                saveItem(index);
                            }, function (error) {
                                console.log(error);
                            });
                    };

                    saveItem(0);
                }, function (error) {
                    console.log(error);
                });
        });
    }

    /**
     * Sync a poa with a new user
     *
     * @param ip        Ip of the POA
     * @param sensorId  Sensor Id to sync
     * @param template  Template of the user
     * @private
     */
    _syncPoa(ip, sensorId, template) {
        var url = `http://${ip}:${POA_PORT}/user/sync`;

        console.log('Sending user, url: ' + url + ' sensorId: ' + sensorId);

        return this.httpService.createRequest(url, 'POST', {sensorId: sensorId, template: template});
    }

    _unSyncPoa(ip, sensorId) {
        var url = `http://${ip}:${POA_PORT}/user/un-sync`;

        console.log('Un-sync user, url: ' + url + ' sensorId: ' + sensorId);

        return this.httpService.createRequest(url, 'POST', {sensorId: sensorId});
    }

    _disconnectNeighbor(ip) {
        this.apiService.disconnect(ip);
    }
}

module.exports = new PoaService();