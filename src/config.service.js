'use strict';

class ConfigService {
    constructor() {
        var fs = require('fs');

        var configPath = `${__dirname}/../config.json`;
        var config = JSON.parse(fs.readFileSync(configPath, 'utf8'));

        this.api = config.api;
        this.server = config.server;
    }
}

module.exports = new ConfigService();