var SensorService = (function () {
    'use strict';

    const MAX_ENROLLED = 200;

    let log = require('winston'),
        async = require('async'),
        GT511C3 = require('gt511c3'),
        sleep = require('sleep');

    let sensorPath = '/dev/ttyAMA0',
        sensorConfig = {
            baudrate: 115200,
            debug: false
        },
        fingerTimeout = 2000,
        /** @var GT511C3*/ sensor;

    constructor();

    return {
        start: start,
        stop: stop,
        enroll: enroll,
        identify: identify,
        getFingerPrint: getFingerPrint,
        setFingerPrint: setFingerPrint,
        removeFingerPrint: removeFingerPrint,
        removeAll: removeAll,
        blink: blink,
        identifyTemplate: identifyTemplate,
        decodeError: decodeError
    };

    function constructor() {

    }

    function decodeError(error) {
        return sensor.decodeError(error);
    }

    function identifyTemplate(template) {
        return sensor.identifyTemplate(template);
    }

    /**
     * Initializes sensor
     */
    function start() {
        sensor = new GT511C3(sensorPath, sensorConfig);

        return sensor.init()
            .then(()=> {
                log.info('Sensor Initialized');
            }, () => {
                log.error(sensor.decodeError(error));
            });
    }

    /**
     * Closes sensor and application
     */
    function stop() {
        console.log('Stopping sensor');

        return sensor.close();
            /*.then(()=> {
                return sensor.close();
            })
            .then(()=> {
                log.info('Sensor stopped');
            });*/
    }

    /**
     * Enrolls a user
     */
    function enroll() {
        let usedId = -1;

        return new Promise((resolve, reject)=> {

            getEmptyId()
                .then((id)=> {
                    usedId = id;
                    return sensor.enrollStart(id);
                })
                .then(()=> {
                    return captureFinger();
                })
                .then(()=> {
                    return sensor.enroll1();
                })
                .then(()=> {
                    console.log('First image created');
                    return sensor.ledONOFF(0)
                })
                .then(()=> {
                    sleep.sleep(2);
                    return captureFinger();
                })
                .then(()=> {
                    return sensor.enroll2();
                })
                .then(()=> {
                    console.log('Second image created');
                    return sensor.ledONOFF(0)
                })
                .then(()=> {
                    sleep.sleep(2);
                    return captureFinger();
                })
                .then(()=> {
                    return sensor.enroll3();
                })
                .then(()=> {
                    console.log('Third image created');
                    return sensor.ledONOFF(0);
                })
                .then(()=> {
                    resolve(usedId);
                })
                .catch((error)=> {
                    console.log(error, sensor.decodeError(error));
                    sensor.ledONOFF(0);
                    reject();
                });
        });
    }

    function releaseFinger() {
        log.info('Release finger');
        return sensor.waitReleaseFinger(fingerTimeout)
            .then(()=> {
                return sensor.ledONOFF(0);
            })
    }

    function captureFinger() {
        return new Promise((resolve, reject)=> {
            var run = function () {
                sensor.ledONOFF(1)
                    .then(()=> {
                        log.info('Please press finger...');
                        return sensor.waitFinger(fingerTimeout);
                    })
                    .then(()=> {
                        log.info('Reading fingerprint...');
                        return sensor.captureFinger();
                    })
                    .then(()=> {
                        resolve();
                    })
                    .catch(()=> {
                        run();
                    });
            };

            run();
        });
    }

    /**
     * Identifies a user
     */
    function identify() {
        var identifiedId;

        return new Promise((resolve, reject)=> {
            sensor.ledONOFF(1)
                .then(()=> {
                    log.info('Please press finger...');
                    return sensor.isPressFinger();
                })
                .then(()=> {
                    log.info('Reading fingerprint...');
                    return sensor.captureFinger();
                })
                .then(()=> {
                    log.info('Identifying fingerprint...');
                    return sensor.identify();
                })
                .then((id)=> {
                    log.info('Fingerprint identified as: ' + id);

                    identifiedId = id;

                    return sensor.ledONOFF(0);
                })
                .then(()=> {
                    resolve(identifiedId);
                })
                .catch((error)=> {
                    if (error == sensor.NACK_IDENTIFY_FAILED) {
                        log.info('Fingerprint not recognized.');
                        reject(sensor.decodeError(error));
                        return;
                    }

                    reject(error);
                });
        });
    }

    /**
     * Finds an available id on the sensor
     * @returns {Promise}
     */
    function getEmptyId() {
        return new Promise((resolve, reject) => {
            var verifyIndex = function (index) {
                if (index >= MAX_ENROLLED) {
                    reject('Database full');
                    return;
                }

                sensor.checkEnrolled(index)
                    .then(() => {
                        verifyIndex(index + 1);
                    }, (error) => {
                        if (error === sensor.NACK_IS_NOT_USED || error === sensor.NACK_DB_IS_EMPTY) {
                            resolve(index);
                        } else {
                            reject(sensor.decodeError(error));
                        }
                    })
            };

            verifyIndex(0);
        });


        return new Promise(function (resolve, reject) {
            let currentId = 0,
                hasFoundId = false;

            async.whilst(()=> {
                return currentId < MAX_ENROLLED && !hasFoundId;
            }, (callback)=> {
                sensor.checkEnrolled(currentId)
                    .then(()=> {
                        currentId++;
                        callback(null, currentId);
                    })
                    .catch((error)=> {
                        if (error === sensor.NACK_IS_NOT_USED) {
                            hasFoundId = true;
                            callback(null, currentId);
                        } else {
                            reject(sensor.decodeError(error));
                            callback(error);
                        }
                    });
            }, (err, id)=> {
                hasFoundId ? resolve(id) : reject('Sensor database full');
            });
        });
    }

    /**
     * Get fingerprint image of the user with the given id
     *
     * @param {Number} id - Id of the user
     * @returns {Object} Image of the fingerprint
     */
    function getFingerPrint(id) {
        log.info(`Retrieving template for id ${id}`);

        return new Promise((resolve, reject) => {
            sensor.ledONOFF(1)
                .then(function () {
                    return sensor.getTemplate(id);
                })
                .then(function (data) {
                    resolve(data);
                }, function (error) {
                    reject(error);
                });
        });
    }

    /**
     * Set fingerprint image of the user with the given id
     *
     * @param {Number} id - Id of the user
     * @param {Object} template - Finger print of the user
     * @returns Image of the fingerprint
     */
    function setFingerPrint(id, template) {
        return sensor.setTemplate(id, template);
    }

    /**
     *
     *
     * @param id
     */
    function removeFingerPrint(id) {
        return sensor.deleteID(id);
    }

    function removeAll() {
        return sensor.deleteAll();
    }

    function blink(nrOfTimes) {
        return new Promise((resolve, reject) => {
            var count = 0;

            var execute = function () {
                sensor.ledONOFF(1)
                    .then(()=> {
                        sleep.sleep(1);
                        return sensor.ledONOFF(0)
                    })
                    .then(()=> {
                        sleep.sleep(1);

                        if (count === nrOfTimes) {
                            resolve();
                            return;
                        }

                        count++;

                        execute();
                    });
            };

            execute();
        });
    }
})();

module.exports = SensorService;