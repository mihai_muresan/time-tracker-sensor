'use strict';

class ExpressServer {
    constructor() {
        this._http = require('http');
        this._express = require('express');
        this._bodyParser = require('body-parser');

        this._config = require('./config.service');
    }

    start() {
        console.log('Starting HTTP server...');

        this._initExpressApp();
        this._initHttpServer();
        this._initControllers();
    }

    addRoute(baseUrl, routes) {
        this._app.use(baseUrl, routes);
    }

    _initExpressApp() {
        var app = this._express();

        app.set('port', this._config.server.port);

        app.use(this._bodyParser.json());
        /*app.use(this._bodyParser.urlencoded({extended: false}));*/

        this._app = app;
    }

    _initHttpServer() {
        var server = this._http.createServer(this._app);

        this._server = server;

        server.listen(this._config.server.port);
        server.on('error', () => this.onError());
        server.on('listening', () => this.onListening());
    }

    onListening() {
        console.log(`HTTP Server started, Listening on: ${this._server.address().port}`);
    }

    onError() {
        console.error(`Could not start HTTP Server`);
        process.exit(1);
    }

    /**
     * @private
     */
    _initControllers() {
         require('./poa/user.controller');
         require('./poa/command.controller');
    }
}

module.exports = new ExpressServer();