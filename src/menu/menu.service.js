var MenuService = (function () {
    'use strict';

    let log = require('winston');

    return {
        print: print
    };

    function print() {
        log.info('\t[i] Clock In');
        log.info('\t[o] Clock Out');
        log.info('\t[r] Register');
        log.info('\t[q] Exit');
    }
})();

module.exports = MenuService;