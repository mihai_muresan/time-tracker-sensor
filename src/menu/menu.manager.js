'use strict';

class MenuManager {
    constructor() {
        this._menuService = require('./menu.service');
        this._poaManager = require('../poa/poa.manager');
    }

    start() {
        this._initAppInput();

        setTimeout(() => this._printMenu(), 100);
    }

    /**
     * Binds the input method of the application
     * @private
     */
    _initAppInput() {
        var stdin = process.stdin;

        if (stdin.isTTY) stdin.setRawMode(true);
        stdin.resume();
        stdin.setEncoding('utf8');

        //TODO show proper messages, colored?

        stdin.on('data', (key)=> {
            this._onButtonPress(key);
        });
    }

    /**
     * Prints the application menu
     * @private
     */
    _printMenu() {
        this._menuService.print();
    }

    /**
     * Event that occurs when a button is pressed
     *
     * @param {String} key Key that was pressed
     * @private
     */
    _onButtonPress(key) {
        switch (key) {

            // Clock In
            case 'i':
                this.sensorService.identify()
                    .then((sensorId)=> {
                        this.log.info(`Creating clock entry for sensorId: ${sensorId}, type: in`);
                        return this.apiService.createClockEntry(sensorId, 'in');
                    })
                    .then((response)=> {
                        this.log.info(`Clock entry created`);
                        console.log(response);
                    });

                break;

            // Clock Out
            case 'o':
                this.sensorService.identify()
                    .then((sensorId)=> {
                        this.log.info(`Creating clock entry for sensorId: ${sensorId}, type: out`);
                        return this.apiService.createClockEntry(sensorId, 'out');
                    })
                    .then((response)=> {
                        console.log(response);
                    });
                break;

            // Enroll user
            case 'r':
                var createdUserSensorId;

                this.sensorService.enroll()
                    .then((sensorId) => {
                        this.log.info(`Creating user for sensorId: ${sensorId}`);
                        createdUserSensorId = sensorId;
                        this.apiService.createUser(sensorId);
                        this.poaService.syncUser(sensorId);
                    });
                break;

            // Close sensor and the application
            case 'q':
                this.sensorService.stop();
                process.exit();
                break;
            case 's':
                var tempTemplate;

                this.sensorService.getFingerPrint(7)
                    .then((template) => {
                        tempTemplate = template;

                        return this.sensorService.removeFingerPrint(7);
                    })
                    .then(() => {
                        this.log(`Setting finger print`);

                        return this.sensorService.setFingerPrint(10, tempTemplate);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
                break
        }
    }
}

module.exports = new MenuManager();